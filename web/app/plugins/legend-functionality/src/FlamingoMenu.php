<?php

namespace Legend\Functionality;

class FlamingoMenu
{

    public function __construct()
    {
        add_action('admin_menu', function () {

            // Only do if Contact Form 7 is active
            if (!is_plugin_active('contact-form-7/wp-contact-form-7.php')) {
                return;
            }

            // More descriptive CF7 menu label
            $wpcf7_menu = $this::getMenuIndexBySlug('wpcf7');
            $GLOBALS['menu'][$wpcf7_menu][0] = __('Forms', 'textdomain');

            // Only do if Flamingo is active
            if (!is_plugin_active('flamingo/flamingo.php')) {
                return;
            }

            // Drop default Flamingo admin menu
            remove_menu_page('flamingo');

            // Add "Inbound Messages" link to CF7 Menu with better menu label
            add_submenu_page(
                'wpcf7',
                __('Flamingo Inbound Messages', 'flamingo'),
                __('Submissions', 'flamingo'),
                'flamingo_edit_inbound_messages',
                'admin.php?page=flamingo_inbound'
            );
        });

        // Fix menu highlighting for Flamingo when moved to CF7 submenu
        add_filter('parent_file', function ($parent_file) {
            if (isset($_GET['page']) && $_GET['page'] == 'flamingo_inbound') {
                $GLOBALS['plugin_page'] = 'wpcf7';
                $GLOBALS['submenu_file'] = 'admin.php?page=flamingo_inbound';
            }

            return $parent_file;
        });
    }

    // Helper function to find admin menu index by slug
    private static function getMenuIndexBySlug($location = '')
    {
        foreach ($GLOBALS['menu'] as $index => $menu_item) {
            if ($location === $menu_item[2]) {
                return $index;
            }
        }
        return false;
    }
}
