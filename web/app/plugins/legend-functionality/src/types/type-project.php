<?php

use PostTypes\PostType;
use PostTypes\Taxonomy;

// Post type options
$options = [
    'supports' => [ 'title', 'thumbnail' ],
    'show_ui' => true,
    'has_archive' => false,
    'rewrite'   => [
        'slug' => 'recent-projects',
        'with_front' => false
    ],
];

// Register post type
$project = new PostType('project', $options);

// Set post type dashicon from Dashicons: https://developer.wordpress.org/resource/dashicons/#chart-bar
$project->icon('dashicons-open-folder');

// Register the post type with WordPress
$project->register();

// Create a new taxonomy
$tax_names = [
    'name' => 'project_category',
    'singular' => 'Category',
    'plural' => 'Categories',
    'slug' => 'project-categories'
];
$project_cat = new Taxonomy($tax_names);

$project_cat->posttype('project');

$project_cat->register();

