<?php
/*
Plugin Name: Legend Functionality
Plugin URI: https://www.legendhasit.co.nz
Description: A starter plugin for Legendary websites.
Version: 1.0
Author: Legend
Author URI: https://www.legendhasit.co.nz
License: MIT
*/

namespace Legend\Functionality;

require __DIR__ . '/vendor/autoload.php';
// Define constants
define('LEGEND_FUNCTIONALITY_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('LEGEND_FUNCTIONALITY_PLUGIN_URL', plugin_dir_url(__FILE__));

// Branding
$brand = new Brand();

// Post Types
 $post_type_builder = new PostTypeBuilder();

// Building default field groups
$acf_config = new ConfigACF();

// Changing the contact form 7 and flamingo admin menu
$flamingo_menu = new FlamingoMenu();
