<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use Log1x\Navi\Navi;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function correctID()
    {
        $id = get_the_id();

        if (is_home()) {
            $id = get_option('page_for_posts');
        }

        return $id;
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_front_page()) {
            return explode(' ', get_bloginfo('description'));
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public function contactInfo()
    {
        return [
            'phone' => get_field('ci_phone', 9),
            'mobile' => get_field('mobile_phone', 9),
            'email' => get_field('ci_email', 9),
            'street_address' => get_field('ci_street_address', 9),
            'postal_address' => get_field('ci_postal_address', 9),
            'hours' => get_field('ci_opening_hours', 9),
            'map' => get_field('ci_map', 9)
        ];
    }

    public function socialMedia()
    {
        return [
            'facebook' => get_field('sm_facebook', 9),
            'instagram' => get_field('sm_instagram', 9)
        ];
    }

    public static function navigation($location)
    {
        $navigation = (new Navi())->build($location);

        if ($navigation->isEmpty()) {
            return;
        }

        return $navigation->toArray();
    }
}
