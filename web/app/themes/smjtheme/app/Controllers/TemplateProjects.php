<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateProjects extends Controller
{
    public function projects()
    {
        $args = [
            'post_type' => 'project',
            'posts_per_page' => -1
        ];

        return new \WP_Query($args);
    }
}
