var $ = require('jquery');
var jQueryBridget = require('jquery-bridget');
var Isotope = require('isotope-layout');

export default {
  init() {
    // JavaScript to be fired on the home page

    // make Isotope a jQuery plugin
    jQueryBridget( 'isotope', Isotope, $ );

    $(window).on('load', function() {
      $('#projectGrid').isotope();
    })

    $('.post-filter .btn').click(function() {
      $('.post-filter .btn').removeClass('text-success').addClass('text-dark');
      var filterValue = $(this).attr('data-filter');
      $('#projectGrid').isotope({ filter: filterValue });
      $(this).addClass('text-success').removeClass('text-dark');
    });


  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
