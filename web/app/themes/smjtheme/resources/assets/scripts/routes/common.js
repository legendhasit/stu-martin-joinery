import 'ekko-lightbox/dist/ekko-lightbox.js';

var $ = require('jquery');
var jQueryBridget = require('jquery-bridget');
var Isotope = require('isotope-layout');

export default {
  init() {
    // JavaScript to be fired on all pages

    $('.sliding-row__toggle, .sliding-row__image').click(function() {
      if (window.outerWidth > 991) {
        $(this).closest('.sliding-row').find('.sliding-row__content').toggleClass('sliding-row__content--collapsed');
      }
    })

    $('.sliding-row').waypoint(function(direction) {
      if(direction == 'down') {
        this.element.firstElementChild.classList.toggle('sliding-row__content--collapsed');
        this.disable();
      }
    }, {
      offset: '50%',
    });

    $('#menuBtn').click(function() {
      $(this).toggleClass('is-active');
      $('body').toggleClass('menu-open');
      if (!$('body').is('.template-showroom, .template-search')) {
        $('.site-header__logo').toggleClass('site-header__logo--dark');
      }
    })

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox();
    });

    $('.process-arrow-lg').each(function(){
        var t = $(this);
        let sub = 272;
        $(window).scroll(function() {
			const h = t.offset().top - ($(window).height() * 80 / 100);
			const hg_scroll = $(window).scrollTop();
			if(hg_scroll > h && sub != 0){
				sub = 272 - (hg_scroll - h);
                if(sub < 0){
                    sub = 0;
                }
                if(t.hasClass('rotate-270')){
                    t.find('svg').css({transform: 'translate(-'+sub+'px, -'+sub+'px) rotate(267deg)'});
                }else{
                    t.find('svg').css({transform: 'translate('+sub+'px, -'+sub+'px)'});
                }
			}
        });
    });

    jQueryBridget( 'isotope', Isotope, $ );

    $(window).on('load', function() {
      $('#projectGrid2').isotope({
          itemSelector: '.grid-item',
          percentPosition: true,
          masonry: {
              columnWidth: '.project2-sizer',
          },
      });

      $('#staffGrid2').isotope({
          masonry: {
              columnWidth: '.staff-item-sizer',
          },
      });

      $('#servicesGrid').isotope({
          masonry: {
              columnWidth: '.service-item-sizer',
          },
      });
    })

    $('.post-filter .btn').click(function() {
      $('.post-filter .btn').removeClass('text-success').addClass('text-dark');
      var filterValue = $(this).attr('data-filter');
      $('#projectGrid2').isotope({ filter: filterValue });
      $(this).addClass('text-success').removeClass('text-dark');
    });


    $('.staff-item .preview__footer, .staff-item .preview__image').on('click', function(){
        var staff = $(this).closest('.staff-item');
        if(!staff.hasClass('expanded')){
            $('.staff-item').removeClass('expanded').css({width: '100%', height: '100%'});
            var ww = $(window).width();
            if(ww > 992){
                var sw = staff.width() * 2 + 1;
                var sh = staff.height() * 2;
                staff.addClass('expanded').css({width: sw + 'px', height: sh + 'px'});
            }else{
                staff.addClass('expanded');
            }
        }
    });

    $('.expanded__close').on('click', function(){
        $(this).closest('.staff-item').removeClass('expanded').css({width: '100%', height: '100%'});
    });

    $('.service-item .preview__footer').on('click', function(){
        var service = $(this).closest('.service-item');
        if(!service.hasClass('expanded')){
            $('.service-item').removeClass('expanded').css({width: '100%', height: '100%'});
            var ww = $(window).width();
            if(ww > 992){
                var sw = service.width() * 2 + 1;
                var sh = service.height() * 2;
                service.addClass('expanded').css({width: sw + 'px', height: sh + 'px'});
            }else{
                service.addClass('expanded');
            }
        }
        // $(this).closest('.service-item').addClass('expanded');
    });

    $('.expanded__close').on('click', function(){
        $(this).closest('.service-item').removeClass('expanded').css({width: '100%', height: '100%'});
    });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
