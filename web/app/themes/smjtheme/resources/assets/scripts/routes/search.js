export default {
  init() {
    // JavaScript to be fired on the home page

    $('.site-header__logo').addClass('site-header__logo--dark')
    $('body').addClass('header-dark no-page-header')

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
