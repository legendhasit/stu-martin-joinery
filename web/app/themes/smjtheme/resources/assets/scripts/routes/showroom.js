import {ImagePanorama, Viewer} from 'panolens';
import TWEEN from '@tweenjs/tween.js';

export default {
  init() {
    // JavaScript to be fired on the home page

    const panoramicImage = document.getElementById('panolensImage').src;
    const panorama = new ImagePanorama( panoramicImage );

    const viewer = new Viewer({
      'container': document.getElementById('panolensContainer'),
      'controlBar': false,
      'horizontalView': true,
      'cameraFov': 120,
    });

    viewer.OrbitControls.noZoom = true;
    viewer.add( panorama );

    $('.site-header__logo').addClass('site-header__logo--dark')
    $('body').addClass('no-page-header');

    $('#pano-button-left').on('click touchstart', function(){
        // RotateLeftRigth(1);
        const coords = { val: 0.05 };
        new TWEEN.Tween(coords)
            .to({ x: 0 }, 300)
            .easing(TWEEN.Easing.Quadratic.Out)
            .onUpdate(() => {
                viewer.OrbitControls.rotateLeft(coords.val);
            })
            .start();
    });
    $('#pano-button-right').on('click touchstart', function(){
        const coords = { val: -0.05 };
        new TWEEN.Tween(coords)
            .to({ x: 0 }, 300)
            .easing(TWEEN.Easing.Quadratic.Out)
            .onUpdate(() => {
                viewer.OrbitControls.rotateLeft(coords.val);
            })
            .start();
    });

    // Setup the animation loop.
    function animate(time) {
        requestAnimationFrame(animate);
        TWEEN.update(time);
    }
    requestAnimationFrame(animate);

  },
  finalize() {

  },
};
