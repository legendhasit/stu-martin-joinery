export default {
  init() {
    // JavaScript to be fired on the home page

    $('.preview__btn').click(function() {
      $('.preview').removeClass('preview--open')
      let clickedPreview = $(this).parent();
      clickedPreview.addClass('preview--open');
    });

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
