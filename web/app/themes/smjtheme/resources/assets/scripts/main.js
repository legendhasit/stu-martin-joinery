// import external dependencies
import 'jquery';
import 'jquery-match-height';
import 'isotope-layout';
import 'waypoints/lib/jquery.waypoints';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import './fontawesome';
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import templateExpertiseHolder from './routes/expertise';
import templateProjects from './routes/all-projects';
import templateShowroom from './routes/showroom';
import templateSearch from './routes/search';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
  templateExpertiseHolder,
  templateProjects,
  templateShowroom,
  templateSearch,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
