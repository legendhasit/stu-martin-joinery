@extends('layouts.app')

@section('content')
  @include('partials.page-header')
  @include('partials.primary-content')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  <div id="news">
    <div class="row no-gutters">
      @while (have_posts()) @php the_post() @endphp
      <div class="col-lg-4 news-preview">
          <a href="{{ get_permalink() }}" class="preview">
            <div class="preview__image">
              {!! the_post_thumbnail('full') !!}
            </div>
            <div class="preview__footer">{!! get_the_title() !!} @svg('arrow-right')</div>
          </a>
      </div>
      @endwhile
    </div>
  </div>

  {!! get_the_posts_navigation() !!}
@endsection
