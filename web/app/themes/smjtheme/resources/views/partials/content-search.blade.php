<article @php post_class('search-result mb-5 pb-3 border-bottom border-secondary') @endphp>
  <header>
    <h3 class="entry-title"><a class="text-decoration-none" href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h3>
  </header>
  <div class="entry-summary">
    @if(get_the_excerpt())
      @php the_excerpt() @endphp

    @elseif(get_field('pc_content'))

      {!! apply_filters('the_excerpt', wp_trim_words(get_field('pc_content'))) !!}

    @else
      {!! function_exists( 'the_seo_framework' ) ? the_seo_framework()->get_description() : '' !!}

    @endif

  </div>
</article>
