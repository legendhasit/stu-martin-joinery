<section class="staggered-row {{ get_field('sg_colour') }}">
    @php
        $sg_row = get_field('sg_row');
    @endphp
  @foreach($sg_row as $key => $item)
    <div class="row no-gutters @if(($key+1)%2 != 0)noteven @endif">
        <div class="col-lg-6 {{ ($loop->iteration % 2 == 0 ? 'order-lg-first' : '') }}">
          <div class="embed-responsive embed-responsive-4by3 h-100">
            {!! wp_get_attachment_image($item['image']['id'], 'full', false, ['class' => 'embed-responsive-item object-cover']) !!}
          </div>
        </div>
      <div class="col-lg-6 d-flex align-items-center justify-content-center">
        <div class="staggered-row__content">
          <h3 class="mb-5 text-uppercase">{!! $item['heading'] !!}</h3>
          {!! $item['content'] !!}
          @if(($key + 1) == count($sg_row))
          @php
            $args = array('post_type' => get_post_type(get_the_ID()), 'posts_per_page' => -1);
            $posts = get_posts($args);
            $first_id = $posts[0]->ID;

            $last_post = end($posts);
            $last_id =  $last_post->ID;

            $prev_post = get_previous_post();
            $prev_url = "";
            if(!empty($prev_post)){
                $prev_url = get_the_permalink($prev_post->ID);
            }else{
                $prev_url = get_the_permalink($first_id);
            }
            $next_post = get_next_post();
            $next_url = "";
            if(!empty($next_post)){
                $next_url = get_the_permalink($next_post->ID);
            }else{
                $next_url = get_the_permalink($last_id);
            }
          @endphp
          <div class="project-arrow">
              <a href="{{$prev_url}}" class="btn btn-arrow arrow-left"><span class="rotate-180">@svg('arrow-left')</span> Go back</a>
              <a href="{{$next_url}}" class="btn btn-arrow">See next @svg('arrow-right')</a>
          </div>
        @endif
        </div>
      </div>
    </div>
  @endforeach
</section>
