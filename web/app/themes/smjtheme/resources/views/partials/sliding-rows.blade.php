@foreach(get_field('sliding_row') as $item)
  <div class="sliding-row">
    <div class="sliding-row__content sliding-row__content--collapsed d-flex justify-content-center align-items-center">
      @svg('dovetail', 'sliding-row__toggle')
      <div class="staggered-row__content">
        <h2 class="text-uppercase mb-5">{!! $item['heading'] !!}</h2>
        {!! $item['content'] !!}
        @if(isset($item['link']['url']))
          <a href="{{ $item['link']['url'] }}" class="btn btn-arrow">{{ $item['link']['title'] }} @svg('arrow-right')</a>
        @endif
      </div>
    </div>
    {!! wp_get_attachment_image($item['gallery']['id'], 'full', false, ['class' => 'sliding-row__image']) !!}
  </div>
@endforeach
