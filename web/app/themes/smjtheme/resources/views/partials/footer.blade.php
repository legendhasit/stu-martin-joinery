@include('partials.testimonials')

<footer class="site-footer bg-dark text-white page-section">
  <div class="container">

    <div class="d-flex justify-content-center">
        @if ($footer_navigation = App::navigation('footer_navigation'))
          <ul class="footer-nav text-center list-unstyled">
            @foreach ($footer_navigation as $item)
              <li>
                <a href="{{ $item->url }}">
                  {{ $item->label }}
                </a>
              </li>
            @endforeach
          </ul>
        @endif
    </div>

    <div class="footer__social text-center">
      <a class="mr-4" target="_blank" rel="noopener" href="{{ $social_media['instagram'] }}"><i class="fab fa-instagram"></i></a>
      <a target="_blank" rel="noopener" href="{{ $social_media['facebook'] }}"><i class="fab fa-facebook-f"></i></a>
    </div>

    <div class="row">
      <div class="order-lg-last col-lg-7 mb-5 mb-lg-0 text-center text-lg-right">
        @svg('award-logo', 'mx-auto mr-sm-4 mb-4 d-block d-sm-inline-block', ['height' => '120'])
        @svg('nkba')
        <img src="@asset('images/master-joiner.png')" alt="Master Joiner" class="d-block d-sm-inline-block img-fluid mx-auto mx-sm-4 my-4 my-sm-0">
        @svg('CDSLogoWhite')
      </div>
      <div class="col-lg-5 d-flex align-items-center justify-content-center justify-content-lg-start">
        <p class="mb-0 font-weight-normal"><a class="text-uppercase mr-4 mr-xl-5 pr-sm-5" href="{{ get_privacy_policy_url() }}">Privacy</a> Copyright &copy; {{ date('Y') }} SMJ</p>
      </div>
    </div>
  </div>
</footer>
