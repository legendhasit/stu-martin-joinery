<a href="{{ $item['image']['url'] }}" data-gallery="showcase-gallery" data-toggle="lightbox" class="{{ $pattern_bg == 4 ? 'h-100' : '' }} showcase__item embed-responsive embed-responsive-16by9">
    @if(!empty($item['image']['ID']))
        {!! wp_get_attachment_image($item['image']['ID'], 'full', false, ['class' => 'embed-responsive-item object-cover']) !!}
    @else
        @if(isset($item['project']->ID))
            {!! get_the_post_thumbnail( $item['project']->ID, 'full', ['class' => 'embed-responsive-item object-cover'] ) !!}
        @endif
    @endif
</a>
@if(isset($item['project']->ID))
  <a href="{{ get_the_permalink($item['project']->ID) }}" class="showcase__project btn btn-outline-light">View Project @svg('arrow-right', 'ml-2')</a>
@endif
