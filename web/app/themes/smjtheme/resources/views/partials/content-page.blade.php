<div class="page-section">
  <div class="container">
    @php the_content() @endphp
  </div>
</div>
