@php
    $post_id = get_the_ID();
    $args = [
        'post_type' => 'project',
        'post__not_in' => [$post_id],
        'posts_per_page' => 3,
        'orderby' => 'rand'
    ];
    $related_projects = new WP_Query($args);
@endphp
@if ($related_projects->have_posts())
<section class="related-projects row no-gutters">
    @while ($related_projects->have_posts())
        @php
            $related_projects->the_post();
        @endphp
        <div class="col-lg-4 kitchens">
            <a href="{{ get_the_permalink( get_the_ID() ) }}" class="preview" title="{{ get_the_title() }}">
                <div class="preview__image">
                    {!! the_post_thumbnail() !!}
                </div>
                <div class="preview__footer"><span class="project-name">{{ get_the_title() }}</span><svg class="" xmlns="http://www.w3.org/2000/svg" width="57.557" height="17.054"><g data-name="Component 15 – 27" fill="none" stroke="currentColor" stroke-width="2"><path data-name="Line 13" d="M0 8.564h55.593"></path><path data-name="Path 197710" d="M47.341.746l8.715 7.781-8.715 7.781"></path></g></svg></div>
            </a>
        </div>
    @endwhile
    @php
        wp_reset_postdata();
    @endphp
</section>
@endif
