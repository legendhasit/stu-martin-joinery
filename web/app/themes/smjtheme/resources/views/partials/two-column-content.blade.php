@if(get_field('tcc_row'))
  <div class="container">
    @foreach(get_field('tcc_row') as $item)
      <section class="page-section border-bottom border-secondary">
        <div class="row justify-content-center">
          <div class="col-12 col-lg-7 text-center mb-5">
            <h3 class="text-uppercase">{!! $item['tcc_heading'] !!}</h3>
          </div>
          <div class="col-lg-6">
            {!! $item['tcc_column_1'] !!}
          </div>
          <div class="col-lg-6">
            {!! $item['tcc_column_2'] !!}
          </div>
        </div>
      </section>
    @endforeach
  </div>
@endif
