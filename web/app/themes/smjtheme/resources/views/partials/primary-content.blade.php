<section class="page-section primary-content">
  <div class="container">
    <div class="row justify-content-between">
      <div class="col-lg-6">
        <h2 class="text-uppercase mb-4">{!! the_field('pc_heading', App::correctID()) !!}<br></h2>
      </div>
      <div class="col-lg-5">
        {!! the_field('pc_content', App::correctID()) !!}
        @if(isset(get_field('pc_link', App::correctID())['url']))
          <a class="btn btn-arrow" href="{{ get_field('pc_link', App::correctID())['url'] }}">{{ get_field('pc_link', App::correctID())['title'] }} @svg('arrow-right')</a>
        @endif
      </div>
    </div>
  </div>
</section>


