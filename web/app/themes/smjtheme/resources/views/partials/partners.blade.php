<section class="page-section">
  <div class="container text-center">
    <h3 class="mb-5">{!! get_field('partners_heading', get_option('page_on_front')) !!}</h3>
    <div class="row justify-content-lg-center mx-n2 mb-n3">
      <div class="col-6 col-md-4 col-lg-3 col-xl px-2 d-flex align-items-center justify-content-center mb-3">
      @foreach(get_field('gallery_rep', get_option('page_on_front')) as $item)
      <div class="object-cover col-6 col-md-4 col-lg-3 col-xl px-2 d-flex align-items-center justify-content-center mb-3">
       <a href="{{$item['link']}}"> <img class="img-fluid" src="{{ $item['image']['sizes']['medium'] }}" alt="Partner"> </a>
      </div>
      @endforeach
    </div>
  </div>
</section>
