<div class="page-header embed-responsive @if (is_home() || is_singular('post') || is_page_template('views/showcase.blade.php')) banner-blog @endif;">
  {!! get_the_post_thumbnail(App::correctID(),'full', ['class' => 'page-header__image embed-responsive-item object-cover']) !!}
  <div class="container h-100 d-flex align-items-center {{ (get_field('title_alignment', App::correctID())) == 'center' ? 'text-center justify-content-center' : '' }}">
    @if(!is_front_page())
      <h1 class="mb-0 animate__animated animate__slideInLeft">{!! App::title() !!}</h1>
    @else
      <div class="d-block w-100">
        @foreach(App::title() as $item)
          <div class="row no-gutters">
            <div class="col-md-4 {{ ($loop->iteration % 2) != 0 ? 'offset-md-4 text-left' : 'text-right' }}">
              <div class="h1 animate__animated {{ ($loop->iteration % 2) != 0 ? 'animate__slideInRight' : 'animate__slideInLeft' }} ">{!! $item !!}</div>
            </div>
          </div>
        @endforeach
      </div>
      <h1 class="sr-only">{!! get_the_title() !!}</h1>
  </div>
    @endif
</div>
@if (is_home() || is_singular('post') || is_page_template('views/showcase.blade.php'))
    <div class="blog-arrow">
        @svg('dovetail', 'sliding-row__toggle')
    </div>
@endif
</div>
