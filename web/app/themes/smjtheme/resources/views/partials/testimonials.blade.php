<div class="page-section testimonials">
  <div class="container">
    <div class="testimonials__quote-icon font-eb-garamond text-center"><span>“</span></div>
    <div id="carouselTestomonials" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        @foreach(get_field('testimonial_item', 2) as $item)
          <div data-mh="carousel-item" class="carousel-item text-center {{ $loop->first ? 'active' : '' }}">
            <div class="row">
              <div class="col-lg-7 mx-auto">
                <h3 class="testimonials__heading mb-4">{!! $item['heading'] !!}</h3>
                <p class="mb-0">{!! $item['content'] !!}</p>
                @if($item['project'])
                  <div class="testimonials__project mt-5">
                    <h6 class="mb-0 pr-4 font-weight-normal">{!! $item['name'] !!}</h6>
                    <a class="testimonials__project-link text-dark text-decoration-none h4 font-eb-garamond font-italic" href="{{ get_permalink($item['project']) }}">
                      <span>View Project</span>
                      @svg('arrow-right', 'ml-3 text-success link-arrow')
                    </a>
                  </div>
                @else
                  <h6 class="mt-5 mb-0">{!! $item['name'] !!}</h6>
                @endif
              </div>
            </div>
          </div>
        @endforeach
      </div>
      <ol class="mt-5 carousel-indicators position-relative">
        @foreach(get_field('testimonial_item', 2) as $item)
          <li data-target="#carouselTestomonials" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
        @endforeach
      </ol>
    </div>
  </div>
</div>
