<header class="site-header">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-102013148-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-102013148-1');
</script>

  <div class="container">
    <div class="row">
      <div class="col-8 col-md-4">
        <a class="d-block site-header__logo" href="{{ home_url('/') }}">
          @svg('logo')
        </a>
      </div>
      <div class="col-4 col-md-8 d-flex align-items-center justify-content-end">
        <a class="d-none d-md-block text-lg-white btn btn-arrow" href="{{ get_permalink(159) }}">@svg('arrow-right', 'mr-4'){!! get_the_title(159) !!}</a>
        <a href="/search" id="searchBtn" class="d-none d-sm-block btn mx-4">
          @svg('magnify')
        </a>
        <button id="menuBtn" class="btn pr-0 hamburger hamburger--vortex" data-toggle="modal" data-target="#navigationModal">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
      </div>
    </div>
  </div>
</header>

@include('partials.modals.navigation-modal')
