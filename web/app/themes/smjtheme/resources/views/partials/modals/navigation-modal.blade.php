{{-- Navigation modal --}}
<div id="navigationModal" class="modal modal-full fade" tabindex="-1" role="dialog" aria-labelledby="Navigation Modal" aria-hidden="true">
  @svg('dovetail', 'd-none d-lg-block')
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="row no-gutters h-100">
        <div class="col-lg-6 p-4 p-md-5 d-flex align-items-center justify-content-center">
          @if ($primary_navigation = App::navigation('primary_navigation'))
            <ul id="primaryNav" class="nav flex-column text-center">
              @foreach ($primary_navigation as $item)
                <li class="nav-item {{ $item->classes ?? '' }}">
                  <a class="nav-link text-uppercase {{ $item->active ? 'active' : '' }}" href="{{ $item->url }}">
                    {{ $item->label }}
                  </a>
                </li>
              @endforeach
            </ul>
          @endif
        </div>
        <div class="col-lg-6 p-4 p-md-5 d-flex align-items-center justify-content-center" style="background: url({{ App\asset_path('images/wood-panels.jpg') }}) no-repeat 50%/cover">
          @if ($secondary_navigation = App::navigation('secondary_navigation'))
            <ul id="secondaryNav" class="nav flex-column text-center">
              @foreach ($secondary_navigation as $item)
                <li class="nav-item {{ $item->classes ?? '' }}">
                  <a class="nav-link text-uppercase {{ $item->active ? 'active' : '' }}" href="{{ $item->url }}">
                    {{ $item->label }}
                  </a>
                </li>
              @endforeach
            </ul>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
