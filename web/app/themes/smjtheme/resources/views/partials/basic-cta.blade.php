<div class="container text-center">
  <div class="row justify-content-center">
    <div class="col-lg-8">
      <h3 class="mb-4">{!! get_field('bcta_heading') !!}</h3>
      @if(isset(get_field('bcta_link')['url']))
        <a href="{{ get_field('bcta_link')['url'] }}" class="btn btn-arrow">{!! get_field('bcta_link')['title'] !!} @svg('arrow-right')</a>
      @endif
    </div>
  </div>
</div>
