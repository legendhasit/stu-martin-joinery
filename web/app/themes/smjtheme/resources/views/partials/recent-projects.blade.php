<section class="recent-projects bg-white">
  <div class="row no-gutters">
    <div class="col-lg-6 col-xl-8 pb-4 pb-lg-0 d-flex align-items-center justify-content-center">
      <div class="recent-projects__bg d-none d-lg-block">
        {!! get_the_post_thumbnail(127, 'full') !!}
      </div>
      <div class="recent-projects__content px-3 pb-5 p-lg-0">
        <h2 class="text-uppercase mb-4">See our<br>latest work</h2>
        <a href="{{ get_permalink(127) }}" class="btn btn-lg btn-dark font-eb-garamond font-italic">Let's take a look @svg('arrow-right', 'ml-3')</a>
      </div>
    </div>
    <div class="col-lg-6 col-xl-4">
      @foreach(get_posts(['numberposts' => 2, 'post_type' => 'project']) as $item)
        <a class="preview" href="{{ get_permalink($item->ID) }}">
          <div class="preview__image">
            {!! get_the_post_thumbnail($item->ID, 'full', ['class' => 'img-fluid']) !!}
          </div>
          <div class="preview__footer">
            {!! get_the_title($item->ID) !!}
            @svg('arrow-right','ml-3')
          </div>
        </a>
      @endforeach
    </div>
  </div>
</section>
