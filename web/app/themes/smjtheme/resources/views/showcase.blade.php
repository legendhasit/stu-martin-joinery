{{--
  Template Name: Showcase
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')

      <div class="page-section showcase pb-0">

          <section class="post-filter mb-5">
            <div class="container text-center">
              <h4 class="font-eb-garamond font-italic mb-4">VIEW  BY</h4>
              @foreach(get_terms('project_category', ['hide_empty' => false]) as $item)
                <button class="btn btn-link text-uppercase text-decoration-none text-dark" data-filter=".{{ $item->slug }}">{{ $item->name }}</button>
                /
              @endforeach
              <button class="btn btn-link text-decoration-none text-dark" data-filter="*">See All</button>
            </div>
          </section>


        <div class="container">
            <div class="mx-n2" id="projectGrid2">
                <div class="project2-sizer"></div>
              @php
                $pattern_bg = 1;
              @endphp
              @foreach(get_field('showcase_gallery') as $item)
                  @php
                      $cat = '';
                      if(!empty($item['category'])){
                          foreach ($item['category'] as $key => $value) {
                              $cat .= $value->slug. ' ';
                          }
                      }
                  @endphp
                @switch($pattern_bg)
                  @case(1)
                      <div class="mt-3 grid-item grid-item--width2 px-2 {{$cat}}">
                        @include('partials.showcase-item')
                      </div>
                    @break

                  @case(2)
                    <div class="mt-3 grid-item px-2 {{$cat}}">
                      @include('partials.showcase-item')
                    </div>
                    @break

                  @case(3)
                    <div class="mt-3 grid-item px-2 {{$cat}}">
                      @include('partials.showcase-item')
                    </div>
                    @break

                  @case(4)
                      <div class="mt-3 grid-item px-2 {{$cat}}">
                        @include('partials.showcase-item')
                      </div>
                      @break

                  @case(5)
                      <div class="mt-3 grid-item px-2 {{$cat}}">
                        @include('partials.showcase-item')
                    </div>
                    @break

                  {{-- @case(6)
                        <div class="mt-3 grid-item grid-item--width2 px-2 {{$cat}}">
                          @include('partials.showcase-item')
                        </div>
                    @break --}}

                @endswitch

                @php
                  $pattern_bg += 1;
                  if($pattern_bg == 6) {
                      $pattern_bg = 1;
                  }
                @endphp

              @endforeach
            </div>
        </div>
      </div>


  @endwhile
@endsection
