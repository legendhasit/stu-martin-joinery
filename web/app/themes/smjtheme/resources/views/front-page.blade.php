@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.sliding-rows')
    @include('partials.recent-projects')
    @include('partials.partners')

  <section>
    <div class="container">
      <div class="text-center">
        <h2 class="text-uppercase">Get inspired</h2>
        <a href="{{ get_permalink(161) }}" class="btn btn-arrow">View our gallery @svg('arrow-right', 'text-dark')</a>
      </div>
    </div>
    <div class="row no-gutters pt-5">
      @foreach(get_field('showcase_gallery', 161) as $item)
        @if($loop->iteration == 3)
          @break
        @endif
        <div class="{{ ($loop->first) ? 'col-md-6 col-lg-8' : 'col' }}">
          <a class="preview" href="{{ get_permalink(161) }}">
            <div class="preview__image">
              {!! wp_get_attachment_image($item['image']['ID'], 'full', false, ['class' => 'img-fluid']) !!}
            </div>
            <div class="preview__footer">
              See more
              @svg('arrow-right','ml-3')
            </div>
          </a>
        </div>
      @endforeach
    </div>
  </section>

  @endwhile
@endsection
