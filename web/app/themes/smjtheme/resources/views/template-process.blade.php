{{--
  Template Name: Process
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    <div class="page-header--arrow-down">
      @svg('long-arrow-down')
    </div>

    <div class="pt-5">
      @include('partials.primary-content')
    </div>

    <section class="staggered-row page-section pt-0 pt-lg-5">
      <div class="container">
        @foreach(get_field('sg_row') as $key => $item)
          <div class="row">
            <div class="col-lg-6 d-flex align-items-center justify-content-center">
              <div class="d-flex flex-wrap">
                <div class="staggered-row__count">{{ $loop->iteration }}</div>
                <div class="staggered-row__content p-3 ">
                  <h3 class="mb-2 text-uppercase">{!! $item['heading'] !!}</h3>
                  {!! $item['content'] !!}
                  @if(isset($item['link']['url']))
                    <a href="{{ $item['link']['url'] }}" class="btn btn-arrow">{{ $item['link']['title'] }} @svg('arrow-right')</a>
                  @endif
                </div>
              </div>
            </div>
            <div class="col-lg-6 d-flex align-items-center justify-content-center {{ ($loop->iteration % 2 != 0 ? 'order-lg-first' : '') }}">
              {!! wp_get_attachment_image($item['image']['id'], 'full', false, ['class' => 'img-fluid']) !!}
            </div>
          </div>
          @if(!$loop->last)
            <div class="text-center d-lg-none my-5 process-arrow-sm arrow-down-{{ $key+1 }}">
              @svg('long-arrow-down')
            </div>
            <div class="text-center d-none d-lg-block my-5 process-arrow-lg arrow-down-{{ $key+1 }} {{ ($loop->iteration % 2 == 0 ? 'rotate-270' : '') }}">
              @svg('long-arrow')
            </div>
          @endif
        @endforeach
      </div>
    </section>

    @include('partials.recent-projects')
  @endwhile
@endsection
