@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.sliding-rows')
    @include('partials.staggered-content')
    @include('partials.related-projects')
  @endwhile
@endsection
