{{--
  Template Name: Search
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

    <section class="search-form-holder">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-8">
            {!! get_search_form(false) !!}
          </div>
        </div>
      </div>
    </section>

  @endwhile
@endsection
