{{--
  Template Name: Contact
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')

  <div class="page-section pb-0">
    <div class="container">
      <div class="row justify-content-between">
        <div class="col-lg-6">
          <h4 class="mb-5">Get in touch, we'd love to help</h4>
          <div class="pl-sm-5">

            <div class="row">
              <div class="col-sm-6">
                <h6 class="font-weight-normal">Phone</h6>
                <a class="font-weight-light text-dark" href="{{ $contact_info['phone']['url'] }}">{{ $contact_info['phone']['title'] }}</a>
                <h6 class="mt-4 font-weight-normal">Visit our showroom</h6>
                <p class="font-weight-light">{!! $contact_info['street_address'] !!}</p>
                <h6 class="mt-4 mb-4 font-weight-normal">Socialise with us on <a target="_blank" rel="noopener" class="pl-2 text-dark" href="{{ $social_media['facebook'] }}"><i class="fab fa-facebook-f"></i></a><a target="_blank" rel="noopener" class="pl-3 text-dark" href="{{ $social_media['instagram'] }}"><i class="fab fa-instagram"></i></a></h6>
              </div>
              <div class="col-sm-6">
                <div class="pl-sm-4">
                  <h6 class="font-weight-normal">Mobile</h6>
                  <a class="font-weight-light text-dark" href="{{ $contact_info['mobile']['url'] }}">{{ $contact_info['mobile']['title'] }}</a>
                  <h6 class="mt-4 font-weight-normal ">Email us</h6>
                  <a class="font-weight-light text-dark" href="mailto:{{ $contact_info['email'] }}">{!! $contact_info['email'] !!}</a>
                </div>
              </div>
            </div>
          </div>

          <div class="mt-5 embed-responsive embed-responsive-16by9">
            {!! $contact_info['map'] !!}
          </div>
        </div>
        <div class="col-lg-5 mt-5 mt-lg-0">
          <h4 class="mb-5">Send us a message now</h4>
          {!! do_shortcode('[contact-form-7 id="5" title="Contact page"]') !!}
        </div>
      </div>
    </div>
  </div>

  @endwhile
@endsection
