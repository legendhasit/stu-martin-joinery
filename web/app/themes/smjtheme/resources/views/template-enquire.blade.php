{{--
  Template Name: Enquire
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

    <section id="showroom" class="showroom subscribe">
      @svg('dovetail')
      <div class="row h-100 no-gutters">
        <div class="col-xl-6 h-100 d-flex align-items-center justify-content-center" style="background-image: url({{ get_the_post_thumbnail_url( get_the_ID(), 'full' ) }}); background-size: cover; ">
          <div class="showroom__content subscribe_left__content">
            <h2 class="text-uppercase mb-4">{!! the_field('pc_heading', App::correctID()) !!}<br></h2>
            {!! the_field('pc_content', App::correctID()) !!}
            @if(isset(get_field('pc_link', App::correctID())['url']))
              <a class="btn btn-arrow" href="{{ get_field('pc_link', App::correctID())['url'] }}">{{ get_field('pc_link', App::correctID())['title'] }} @svg('arrow-right')</a>
            @endif
          </div>
        </div>
        <div class="col-xl-6 d-flex align-items-center justify-content-center enquire-right">
            <div class="enquire-form">
                {!! do_shortcode( '[contact-form-7 id="'.get_field("select_form").'" title="Enquire Form"]' ) !!}
            </div>
        </div>
      </div>
    </section>


  @endwhile
@endsection
