{{--
  Template Name: Projects
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.primary-content')

    <section class="post-filter mb-5">
      <div class="container text-center">
        <h4 class="font-eb-garamond font-italic mb-4">VIEW  BY</h4>
        @foreach(get_terms('project_category') as $item)
          <button class="btn btn-link text-uppercase text-decoration-none text-dark" data-filter=".{{ $item->slug }}">{{ $item->name }}</button>
          /
        @endforeach
        <button class="btn btn-link text-decoration-none text-dark" data-filter="*">See All</button>
      </div>
    </section>

    <section id="projectGrid" class="row no-gutters">

      @while($projects->have_posts())
       @php
        $projects->the_post();
       @endphp
      <div class="col-lg-4 {{ join(' ', wp_list_pluck(get_the_terms(get_the_ID(), 'project_category'), 'slug')) }}">
        <a href="{{ get_the_permalink() }}" class="preview">
          <div class="preview__image">
            {!! get_the_post_thumbnail(null, 'full') !!}
          </div>
          <div class="preview__footer"><span class="project-name">{!! the_title() !!}</span> @svg('arrow-right')</div>
        </a>
      </div>
      @endwhile
    </section>

  @endwhile
@endsection
