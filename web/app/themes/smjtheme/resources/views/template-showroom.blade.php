{{--
  Template Name: Showroom
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

    <section id="showroom" class="showroom">
      @svg('dovetail')
      <div class="row h-100 no-gutters">
        <div class="col-xl-6 h-100 d-flex align-items-center justify-content-center">
          <div class="showroom__content">
            <h2 class="text-uppercase mb-4">{!! the_field('pc_heading', App::correctID()) !!}<br></h2>
            {!! the_field('pc_content', App::correctID()) !!}
            @if(isset(get_field('pc_link', App::correctID())['url']))
              <a class="btn btn-arrow" href="{{ get_field('pc_link', App::correctID())['url'] }}">{{ get_field('pc_link', App::correctID())['title'] }} @svg('arrow-right')</a>
            @endif
          </div>
        </div>
        <div class="col-xl-6 d-flex align-items-center justify-content-center">
          <div id="panolensContainer" class="h-100 w-100">
            <img id="panolensImage" src="@if(get_field("pino_image")) {{ get_field('pino_image') }} @else @asset('images/showroom.jpg') @endif" alt="Showroom image" class="d-none">
            <button id="pano-button-left"><img src="@asset('images/arrow-left.png')" alt="icon" /></button>
            <button id="pano-button-right"><img src="@asset('images/arrow-right.png')" alt="icon" /></button>
          </div>
        </div>
      </div>
    </section>




  @endwhile
@endsection
