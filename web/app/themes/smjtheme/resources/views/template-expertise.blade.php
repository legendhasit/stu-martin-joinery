{{--
  Template Name: Expertise
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.primary-content')
    @include('partials.staggered-content')
    @include('partials.two-column-content')
    @include('partials.partners')
    @include('partials.recent-projects')
  @endwhile
@endsection
