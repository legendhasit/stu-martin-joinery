{{--
  Template Name: Who we are
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.primary-content')

    <section id="staffGrid" class="page-section pt-0">
      <div class="container text-center">
        <h3 class="mb-5">Meet Our Team</h3>
      </div>
      {{-- <div id="staffGrid2" class="staff-grid2">
          <div class="staff-item-sizer">

          </div>
        @foreach(get_field('team_member') as $key => $item)
          <div class="staff-item">
            <div class="preview">
                <button class="expanded__close">@svg('close')</button>
              <div class="preview__image">
                {!! wp_get_attachment_image($item['image']['ID'], 'full') !!}
              </div>
              <div class="preview__footer">
                  <span class="staff-name">{!! $item['name'] !!} @svg('arrow-right')</span>
                  <div class="staff-info">
                      <div class="row">
                        <div class="col-lg-4">
                          <h3>Meet {!! explode(' ', $item['name'], 2)[0] !!}</h3>
                        </div>
                        <div class="col-lg-8">
                            <div class="content-right">
                                {!! $item['description'] !!}
                                <h5 class="font-eb-garamond font-italic">{!! $item['name'] . ', ' . $item['position'] !!}</h5>
                            </div>
                        </div>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        @endforeach
      </div> --}}
      <div id="staffGrid" class="row no-gutters">
          @php
          $team_member = get_field('team_member');
          $count = 0;
          if(is_array($team_member)){
              $count = count($team_member);
              $is = (ceil($count / 3) - 1) * 3;
          }
          @endphp
        @foreach(get_field('team_member') as $key => $item)
          <div class="col-lg-4">
            <div class="preview staff-item @if($key >= $is) bottom0  @endif" >
                <button class="expanded__close">@svg('close')</button>
                <div class="preview__image">
                    {!! wp_get_attachment_image($item['image']['ID'], 'full') !!}
                </div>
                <div class="preview__footer">
                    <span class="staff-name">{!! $item['name'] !!} @svg('arrow-right')</span>
                    <div class="staff-info">
                        <div class="row">
                            <div class="col-lg-4">
                                <h3>Meet {!! explode(' ', $item['name'], 2)[0] !!}</h3>
                            </div>
                            <div class="col-lg-8">
                                <div class="content-right">
                                    {!! $item['description'] !!}
                                    <h5 class="font-eb-garamond font-italic">{!! $item['name'] . ', ' . $item['position'] !!}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        @endforeach
      </div>
    </section>

    @foreach(get_field('team_member') as $item)
      <!-- Modal -->
      <div class="modal fade staff-modal" id="staffModal{{ $loop->index }}" tabindex="-1" aria-labelledby="staffModal{{ $loop->index }}" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
          <div class="modal-content border-0">
            <button data-dismiss="modal" class="modal__close">@svg('close')</button>
            <div class="modal-body p-0">
              {!! wp_get_attachment_image($item['image']['ID'], 'full', false, ['class' => 'img-fluid']) !!}
              <div class="p-4 p-lg-5">
                <div class="row">
                  <div class="col-lg-4">
                    <h3>Meet {!! explode(' ', $item['name'], 2)[0] !!}</h3>
                  </div>
                  <div class="col-lg-8">
                    {!! $item['description'] !!}
                    <h5 class="font-eb-garamond font-italic">{!! $item['name'] . ', ' . $item['position'] !!}</h5>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer p-3 border-secondary d-sm-none">
              <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    @endforeach


    @include('partials.basic-cta')
    @include('partials.partners')
    @include('partials.recent-projects')

  @endwhile
@endsection
