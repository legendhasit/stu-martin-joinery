{{--
  Template Name: Expertise Holder
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.primary-content')

    <section id="servicesGrid2" class="services-Grid2 row no-gutters">
        @php
        $services = get_children(['post_parent'=> get_the_id(), 'post_type' => 'page']);
        $count = 0;
        if(is_array($services)){
            $count = count($services);
            $is = (ceil($count / 3) - 1) * 3;
        }
        $i = 0;
        @endphp
      @foreach( $services as $item)
          <div class="col-lg-4">
            <div class="service-item @if($i >= $is) bottom0  @endif">
              <a href="{{ get_permalink($item->ID) }}" class="preview preview--overlay">
                <div  class="preview__image">
                  {!! get_the_post_thumbnail($item->ID, 'full') !!}
                </div>
                <div class="preview__overlay">
                  <h3 class="mb-0">{!! get_the_title($item->ID) !!}</h3>
                </div>
              </a>
              <div class="service-info">
                  <button class="expanded__close">@svg('close')</button>
                  <div class="service-content">
                      <h3>{!! get_field('pc_heading', $item->ID) !!}</h3>
                      {!! apply_filters('the_excerpt', wp_trim_words(get_field('pc_content', $item->ID))) !!}
                      <a href="{{ get_permalink($item->ID) }}" class="btn btn-arrow">See more @svg('arrow-right')</a>
                  </div>
              </div>
              <div class="preview__footer">Take a peek @svg('arrow-right')</div>
            </div>
        </div>
        @php
            $i++;
        @endphp
      @endforeach
    </section>

  @foreach(get_children(['post_parent'=> get_the_id(), 'post_type' => 'page']) as $item)
    <!-- Modal -->
    <div class="modal fade expertise-modal" id="expertiseModal{{ $loop->index }}" tabindex="-1" aria-labelledby="expertiseModal{{ $loop->index }}" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content border-0">
          <button data-dismiss="modal" class="modal__close">@svg('close')</button>
          <div class="modal-body p-0">
              <div class="row no-gutters">
                <div class="col-lg-6 col-xl-7">
                {!! get_the_post_thumbnail($item->ID, 'full', ['class' => 'expertise-modal__image']) !!}
                </div>
                <div class="col-lg-6 col-xl-5 p-4 p-xl-5 my-lg-3">
                  <h3>{!! get_field('pc_heading', $item->ID) !!}</h3>
                  {!! apply_filters('the_excerpt', wp_trim_words(get_field('pc_content', $item->ID))) !!}
                  <a href="{{ get_permalink($item->ID) }}" class="btn btn-arrow">See more @svg('arrow-right')</a>
                </div>
              </div>
          </div>
          <div class="modal-footer p-3 border-secondary d-sm-none">
            <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  @endforeach

    @include('partials.partners')
    @include('partials.recent-projects')

  @endwhile
@endsection
